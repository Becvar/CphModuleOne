package Week36.Controllers;

import Libs.Form.Form;

/**
 * Created by becva on 07.09.2016.
 */
public class IntegerAdder {
    private static Form form = new Form();

    public static void main(String[] args) {
        createForm();
        if(form.size() > 1) {
            System.out.println("The sum of " + getInputsString() + " is " + getInputsSum());
        } else if(form.size() == 1) {
            System.out.println("The only value is "+getInputsSum());
        } else {
            System.out.println("No value.");
        }
    }

    private static void createForm() {
        int i = 0;
        while(true) {
            form.addNumber("number"+i,"Enter integer (or break the loop either with '0' or hitting enter)")
                    .setDefaultValue("0");
            form.askForField("number"+i);
            if((int) form.getNumber("number"+i).getValue() == 0) {
                break;
            }
            i++;
        }
    }
    private static String getInputsString() {
        String result = "";
        String separator = "";
        int length = form.size();
        for (int i = 0; i<length;i++) {
            result += separator+(int) form.getNumber("number"+i).getValue();
            separator = ", ";
            if(i == length-2) {
                separator = " and ";
            }
        }
        return result;
    }
    private static int getInputsSum() {
        int sum = 0;
        for (int i = 0; i<form.size();i++) {
            sum += form.getNumber("number"+i).getValue();
        }
        return sum;
    }
}