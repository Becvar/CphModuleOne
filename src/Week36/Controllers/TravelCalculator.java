package Week36.Controllers;

import Libs.Form.Form;
import Libs.NumberPartioner;

import java.util.LinkedHashMap;

/**
 * Created by becva on 06.09.2016.
 */
public class TravelCalculator {
    private static Form form;
    static {
        form = new Form();
        form.addNumber("distance","distance")
                .setDefaultValue("500");
        form.addText("distanceUnit","distance unit")
                .setAllowedValues(new String[]{"km", "M", "m"})
                .setDefaultValue("km");
        form.addNumber("speed","speed")
                .setDefaultValue("120");
        form.addText("speedUnit","speed unit")
                .setAllowedValues(new String[]{"s", "h"})
                .setDefaultValue("h");
    }

    public static void main(String[] args) {
        form.askForAllFields();
        int seconds = getTravelDuration();
        printTravelDuration(seconds);
        printTimeParts(seconds);
    }
    private static int getTravelDuration(){
        float distance = form.getNumber("distance").getValue();
        float speed = form.getNumber("speed").getValue();
        int multiplier = 1;
        if (form.get("speedUnit").toString().equals("h")) {
            multiplier = 3600;
        }
        float travelTime = distance / speed;
        float seconds = travelTime * multiplier;
        return (int) seconds;
    }
    private static void printTravelDuration(int seconds) {
        System.out.println("Travel time for "+
                form.get("distance")+" "+form.get("distanceUnit")+
                ", with the speed of "+
                form.get("speed")+" "+form.get("distanceUnit")+"/"+form.get("speedUnit")+
                " is "+seconds+" seconds.");
    }
    private static void printTimeParts(int seconds) {
        // create a mapper for time partioning (it's a type of array)
        LinkedHashMap<String, Integer> timePartsMapper = new LinkedHashMap<String, Integer>();
        timePartsMapper.put("hours",3600);
        timePartsMapper.put("minutes",60);
        timePartsMapper.put("seconds",1);
        String[] unitMapper = {"hours","minutes","seconds"};
        int[] amountMapper = {3600,60,1};
        System.out.println(new NumberPartioner(seconds,unitMapper,amountMapper,"seconds"));
    }
}