package Week36.Controllers;

import Libs.Form.Form;

/**
 * Created by becva on 13.09.2016.
 */
public class NumericalConverter {
    private static Form form = new Form();

    public static void main(String[] args) {
        createForm();
    }

    private static void createForm() {
        form.addNumber("systemFrom","Insert numerical system to convert from")
                .setRange(2,36);
        form.addNumber("number","Insert the number to convert")
                .setRange(0,Float.MIN_VALUE);
        form.addNumber("systemTo","Insert numerical system to convert to")
                .setRange(2,36);
    }
}
