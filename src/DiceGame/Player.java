package DiceGame;

import Libs.Form.Form;
import Libs.Form.Inputs.NumberField;

import java.util.LinkedList;

/**
 * Created by becva on 19.09.2016.
 */
public class Player {
    LinkedList<Bet> bets = new LinkedList<Bet>();
    private static Form betForm = new Form();
    static {
        betForm.addNumber("amount","How much do you want to bet?")
            .setDefaultValue("1");
    }
    String name;
    float score = 100;

    public Player(String name) {
        this.name = name;
    }

    public Bet createBet() {
        prepareBetFormAndAsk();
        float bettedAmount = betForm.getNumber("amount").getValue();
        Bet bet = Better.createBet(bettedAmount);
        bets.add(bet);
        return bet;
    }
    private void prepareBetFormAndAsk(){
        betForm.getNumber("amount")
                .setRange(1,score);
        betForm.askForField("amount");
    }

    public void calculateScore(float wonAmount) {
        System.out.println(wonAmount);
        score += wonAmount;
    }

    public void printRoundStats() {
        Bet bet = bets.getLast();
        float multiplier = bet.getMultiplier();
        float wonAmount = bet.getWonAmount();
        if(multiplier != 0) {
            System.out.println(name+" won a bet and earns "+wonAmount+" point/s");
        } else {
            System.out.println(name+" lost the bet and looses "+betForm.getNumber("amount").getValue() + " point/s");
        }
    }

    public void printStats() {
        System.out.println("......................player "+name+"..........................");
        float betSum = 0;
        float rewardSum = 0;
        int wonRounds = 0;
        float reward;
        float userBet;
        int rounds = bets.size();
        for (int i = 0; i < rounds; i++) {
            userBet = bets.get(i).userBet;
            betSum += userBet;
            reward = bets.get(i).getWonAmount()-userBet;
            rewardSum += reward;
            if(reward > 0) {
                wonRounds++;
            }
            System.out.println("Round "+(i+1));
            System.out.println(bets.get(i));
        }
        System.out.println("Final score: "+score);
        System.out.println("Average bet: "+(betSum/rounds));
        System.out.println("Average reward: "+(rewardSum/rounds));
        System.out.println("Won Rounds: "+wonRounds);
        System.out.println("Lost Rounds: "+(rounds - wonRounds));
    }
}
