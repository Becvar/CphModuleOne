package DiceGame;

import Libs.Form.Form;

import java.util.ArrayList;

/**
 * Created by becva on 21.09.2016.
 */
public class PlayerFactory {

    public static ArrayList<Player> getUsers() {
        Form playerForm = getPlayerForm();
        ArrayList<Player> players = new ArrayList<>();
        String continueAdding = "y";
        System.out.println(".......................ADDING_PLAYERS...............................");
        while (wantToContinue(continueAdding)) {
            String userName = playerForm.askForField("userName");
            players.add(new Player(userName));
            System.out.println("Player named: "+userName+" was added\n");
            continueAdding = playerForm.askForField("continueAdding");
        }
        return players;
    }
    private static Form getPlayerForm() {
        Form playerForm = new Form();
        playerForm.addText("userName","Insert user's name")
                .setDefaultValue("User");
        playerForm.addContinue("continueAdding","Do you want to continue adding users?")
                .setDefaultValue("n");
        return playerForm;
    }
    private static boolean wantToContinue(String continueAdding) {
        return continueAdding.substring(0,1).equalsIgnoreCase("y");
    }
}
