package DiceGame;

import Libs.Form.Form;

import java.util.ArrayList;

/**
 * Created by becva on 21.09.2016.
 */
public class Better {
    private static ArrayList<int[]> betRules = new ArrayList<>();
    static {
        betRules.add(new int[]{2,3,11,12});
        betRules.add(new int[]{4,5,9,10});
        betRules.add(new int[]{6,7,8});
    }
    private static float[] betMultipliers = new float[]{(float) 1.5,2,3};
    private static Form guessForm = new Form();
    static {
        guessForm.addNumber("guess1","What is your first quess?")
                .setRange(1,6)
                .setDefaultValue("1");
        guessForm.addNumber("guess2","What is your second quess?")
                .setRange(1,6)
                .setDefaultValue("1");
    }

    public static Bet createBet(float userBet) {
        Bet bet = new Bet(userBet);
        guessForm.askForAllFields();
        bet.setGuesses(guessForm.getAllNumberInputs());
        return bet;
    }

    public static void setMultiplier(Bet bet) {
        if (!bet.computerThrow.isSameAs(guessForm.getAllNumberInputs())) {
            return;
        }
        for (int multiplierKey = 0; multiplierKey < betRules.size(); multiplierKey++) {
            for (int rule : betRules.get(multiplierKey)) {
                if(bet.computerThrow.getSum() == rule) {
                    bet.setMultiplier(betMultipliers[multiplierKey]);
                    return;
                }
            }
        }
    }
}
