package DiceGame;

import Libs.Form.Form;

import java.util.ArrayList;

/**
 * Created by becva on 19.09.2016.
 */
public class Bet {
    private ArrayList<Float> guesses;
    private float multiplier = 0;
    float userBet;
    public Throw computerThrow;

    public Bet(float userBet) {
        this.userBet = userBet;
    }

    public void setGuesses(ArrayList<Float> guesses) {
        this.guesses = guesses;
    }

    public String toString() {
        return
            getGuessesStats()+
            computerThrow.getThrownStats()+
            getBetStats();
    }
    private String getGuessesStats() {
        String guessesStats = "";
        for (int i = 0; i < this.guesses.size(); i++) {
            guessesStats += "guess "+(i+1)+"............"+this.guesses.get(i).intValue()+"\n";
        }
        return guessesStats;
    }
    private String getBetStats() {
        String betStats = "user bet..........."+userBet+"\n";
        if(multiplier == 0) {
            betStats += "lost..............."+userBet+"\n";
        } else {
            betStats += "won................"+getWonAmount()+"\n";
        }
        return betStats;
    }

    public void setComputerThrow(Throw computerThrow) {
        this.computerThrow = computerThrow;
    }

    public void setMultiplier(float multiplier) {
        this.multiplier = multiplier;
    }

    public float getMultiplier() {
        return multiplier;
    }

    public float getWonAmount() {
        return userBet*multiplier;
    }
}
