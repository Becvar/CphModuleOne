package DiceGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by becva on 19.09.2016.
 */
public class Throw {
    private static Random random = new Random();
    ArrayList<Float> thrown = new ArrayList<>();
    int sum = 0;

    public Throw() {
        System.out.println("\n................throwing.................\n");
        for (int i = 0; i < DiceGame.THROW_AND_GUESS_NUMBER; i++) {
            thrown.add((float) random.nextInt(6)+1);
        }
        setSum();
        System.out.println("thrown sum: "+sum);
    }
    public boolean isSameAs(ArrayList<Float> guesses) {
        for (Object thrownNumber : thrown) {
            if (!guesses.remove(thrownNumber)) {
                return false;
            }
        }
        return guesses.isEmpty();
    }
//    public boolean isSameAs() {
//        return guesses.containsAll(thrown);
//    }

    private void setSum() {
        for (float throwedNumber : thrown) {
            sum += throwedNumber;
        }
    }

    public int getSum() {
        return sum;
    }

    public String getThrownStats() {
        String thrownStats = "";
        for (int i = 0; i < thrown.size(); i++) {
            thrownStats += "computer throw "+(i+1)+"..."+thrown.get(i).intValue()+"\n";
        }
        return thrownStats;
    }
}
