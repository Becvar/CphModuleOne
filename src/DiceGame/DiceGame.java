package DiceGame;

import Libs.Form.Form;

import java.util.ArrayList;

/**
 * Created by becva on 19.09.2016.
 */
public class DiceGame {
    private static ArrayList<Player> players = new ArrayList<Player>();
    private static ArrayList<Throw> computerThrows = new ArrayList<Throw>();
    public static final int THROW_AND_GUESS_NUMBER = 2;
    private static int rounds = 0;
    private static Form continueForm = new Form();
    static {
        continueForm.addContinue("continue","Do you want to continue the game?")
            .setDefaultValue("y");
    }

    public static void main(String[] args) {
        players = PlayerFactory.getUsers();
        while (noBodyLostAllScore() && wantToContinue()){
            rounds++;
            Throw computerThrow = createComputerThrow();
            for (Player player : players) {
                doTheBetting(computerThrow, player);
                player.printRoundStats();
                reCalculateScores(player);
            }
        }
        printFinalStats();
    }
    private static boolean noBodyLostAllScore() {
        boolean continueGame = true;
        for (Player player : players) {
            if(continueGame) {
                continueGame = player.score > 0;
            }
        }
        return continueGame;
    }
    private static boolean wantToContinue() {
        if(rounds == 0) {
            return true;
        }
        String wantContinue = continueForm.askForField("continue");
        return wantContinue.substring(0,1).equalsIgnoreCase("y");
    }
    private static Throw createComputerThrow() {
        Throw computerThrow = new Throw();
        computerThrows.add(computerThrow);
        return computerThrow;
    }
    private static void doTheBetting(Throw computerThrow, Player player) {
        System.out.println("\n......................"+player.name+"s turn.............................\n");
        Bet bet = player.createBet();
        bet.setComputerThrow(computerThrow);
        Better.setMultiplier(bet);
    }
    private static void reCalculateScores(Player bettingPlayer) {
        Bet bet = bettingPlayer.bets.getLast();
        float wonAmount = bet.getWonAmount()-bet.userBet;
        bettingPlayer.calculateScore(wonAmount);
        float scrapeAmount = wonAmount/(players.size()-1)*(-1);
        for (Player player : players) {
            if (player != bettingPlayer) {
                player.calculateScore(scrapeAmount);
            }
        }
    }

    private static void printFinalStats() {
        System.out.println("\n.......................FINAL STATS for "+rounds+" round/s.................\n");
        for (Player player : players) {
            player.printStats();
        }
        int sumSum = 0;
        for (Throw computerThrow : computerThrows) {
            sumSum += computerThrow.sum;
        }
        System.out.println("\nAverage thrown sum: "+sumSum/rounds);
    }
}
