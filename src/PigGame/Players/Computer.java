package PigGame.Players;

import PigGame.Dice;
import PigGame.PigGame;

/**
 * Created by becva on 16.09.2016.
 */
public class Computer extends Player {
    private static int computerCount = 0;
    private int storeAt;

    public Computer(Dice dice,int storeAt) {
        name = "Computer"+(++computerCount);
        this.dice = dice;
        this.storeAt = storeAt;
    }

    public void doTurn() {
        int throwPower = rand.nextInt(10)+1;
        int bet = 0;
        if(!wantToContinue()) {
            bet = 1;
        }
        doThrow(throwPower,bet);
    }
    protected boolean wantToContinue() {
        boolean savingWontBringLead = turnScore + bankScore <= winningScore;
        if(!winners.isEmpty() && savingWontBringLead) {
            return true;
        }
        return turnScore < storeAt && savingWontBringLead;
    }
}
