package PigGame.Players;

import PigGame.Dice;
import PigGame.PigGame;
import PigGame.Better;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by becva on 16.09.2016.
 */
abstract public class Player {
    static Random rand = new Random();
    static int winningScore = PigGame.winningScore;
    public static LinkedList<Player> winners = new LinkedList<Player>();

    Dice dice;
    // TODO: figure out more elegant solution
    private Better better = new Better(this);
    private int throwsCount = 0;
    private int lostTurnScore = 0;
    private int lostBankScore = 0;
    public int bankScore = 0;
    public int turnScore = 0;
    public int throwScore = 0;
    public String name;
    public boolean oneInTheFirst = false;

    public abstract void doTurn();
    protected abstract boolean wantToContinue();

    void doThrow(int throwPower, int bet) {
        throwsCount++;
        throwScore = dice.throwAndGetResult(throwPower);
        better.handleBet(bet);
        turnScore += throwScore;
    }

    public boolean checkForInteruptionAndReport(int throwAttempt) {
        if(throwAttempt == 1) {
            oneInTheFirst = throwScore == 1;
            if(oneInTheFirst) {
                eraseTurnScoreAndDisplay();
            }
        } else {
            if(throwScore == 1){
                eraseTurnScoreAndDisplay();
                if(oneInTheFirst) {
                    eraseBankScoreAndDisplay();
                }
                wipeTurn();
                return true;
            }
            displayThrowEnd();
            if(oneInTheFirst || !wantToContinue()) {
                saveInTheBankAndDisplay();
                return true;
            }
        }
        return false;
    }
    private void eraseTurnScoreAndDisplay() {
        lostTurnScore += turnScore;
        wipeTurn();
        System.out.println(name + "s turn score was erased and his turn will be ended (because of '1')");
    }
    private void eraseBankScoreAndDisplay() {
        lostBankScore += bankScore;
        bankScore = 0;
        System.out.println("also "+name + "s bank score was erased (because of double '1')");
        displayThrowEnd();
    }
    private void wipeTurn() {
        turnScore = 0;
        throwScore = 0;
    }
    private void displayThrowEnd() {
        System.out.println("\n"+name+"s turn score (not in the bank): "+turnScore);
    }

    // TODO: round skipping because of lost bet, or something of that kind
    public boolean initializePlayerRound(int round) {
        System.out.println("\n....................."+name+"s TURN number "+(round)+"..............\n");
        return true;
    }

    private void saveInTheBankAndDisplay() {
        bankScore += turnScore;
        setWinnerIfExists();
        System.out.println(name + " is adding "+turnScore+" points. Score in the bank is now: "+bankScore);
        wipeTurn();
    }
    private void setWinnerIfExists() {
        if(bankScore > winningScore) {
            winners.clear();
            winners.add(this);
            winningScore = bankScore;
        } else if(bankScore == winningScore) {
            winners.add(this);
        }
    }

    public void printStats() {
        System.out.println("\n"+name+":");
        System.out.println("Total throws..............."+throwsCount);
        System.out.println("Bank Score................."+bankScore);
        System.out.println("Lost Bank Score............"+lostBankScore);
        System.out.println("Lost Turn Score............"+lostTurnScore);
        better.printStats();
    }
}