package PigGame.Players;

import Libs.Form.Form;
import PigGame.Dice;

/**
 * Created by becva on 16.09.2016.
 */
public class User extends Player {
    private static Form form;
    static {
        form = new Form();
        form.addNumber("throwPower","Select throw power")
                .setRange(1,10)
                .setDefaultValue("1");
        form.addNumber("bet","\nBet on number you think you'll throw" +
                "\n0 = skip bet" +
                "\n1 = ignore current throw score and effect (impossible to get double '1', possible to get single '1')" +
                "\n3-6 = double throw score on success, decrease thrown score by 1 on failure")
                .setAllowedValues(new String[]{"0","1","3","4","5","6"})
                .setDefaultValue("0");
        form.addText("continue","Do you want to continue throwing? (No = store turn score in the bank)")
                .setAllowedValues(new String[]{"y","n","Y","N","Yes","No","yes","no"})
                .setDefaultValue("y");
    }

    public User(String name, Dice dice) {
        this.name = name;
        this.dice = dice;
    }

    public void doTurn() {
        form.askForField("bet");
        form.askForField("throwPower");
        int throwPower = (int) form.getNumber("throwPower").getValue();
        int bet = (int) form.getNumber("bet").getValue();
        doThrow(throwPower,bet);
    }

    protected boolean wantToContinue() {
        if(turnScore > 0) {
            String shouldContinue = form.askForField("continue").substring(0, 1);
            return shouldContinue.equalsIgnoreCase("Y");
        } else {
            return true;
        }
    }
}
