package PigGame;

import PigGame.Players.Player;

/**
 * Created by becva on 18.09.2016.
 */
public class Better {
    private int scoreWonInBets = 0;
    private int scoreLostInBets = 0;
    private int bankScoreSavedInBets = 0;
    private int turnScoreSavedInBets = 0;
    private Player player;
    
    public Better(Player player) {
        this.player = player;
    }

    public void handleBet(int bet) {
        if (bet != 0) {
            System.out.print("But "+player.name + " bet number " + bet);
            if (bet == 1) {
                if (bet == player.throwScore) {
                    setSavedScoreAndReport();
                } else {
                    setWastedScoreAndReport();
                }
                player.throwScore = 0;
            } else{
                if (bet == player.throwScore) {
                    setWonScoreAndReport();
                } else if(player.throwScore != 1){
                    setLostScoreAndReport();
                } else {
                    System.out.print(" but it didn't help one bit, since he/she threw '1'\n");
                }
            }
        }
    }
    private void setSavedScoreAndReport() {
        if(player.oneInTheFirst) {
            bankScoreSavedInBets += player.bankScore;
            turnScoreSavedInBets += player.turnScore;
            System.out.print(" and didn't get bank and turn score erased, good work :)\n");
        } else {
            turnScoreSavedInBets += player.turnScore;
            System.out.print(" and didn't get turn score erased, nor will be his/hers round interrupted, good work :)\n");
        }
    }
    private void setWastedScoreAndReport() {
        scoreLostInBets += player.throwScore;
        System.out.print(" and wasted all "+player.throwScore+" thrown points :(\n");
    }
    private void setWonScoreAndReport() {
        scoreWonInBets = player.throwScore;
        System.out.print(" and earned " + player.throwScore + " more points :)\n");
        player.throwScore *= 2;
    }
    private void setLostScoreAndReport() {
        if(player.throwScore != 2) {
            player.throwScore--;
            scoreLostInBets++;
            System.out.print(" and lost 1 point :(\n");
        } else {
            System.out.print(" and he/she was a one lucky player, because when number 2 is thrown, no points are lost\n");
        }
    }

    public void printStats() {
        System.out.println("Raw score won in Bets......"+scoreWonInBets);
        System.out.println("Raw score lost in Bets....."+scoreWonInBets);
        System.out.println("Turn score saved in Bets..."+turnScoreSavedInBets);
        System.out.println("Bank score saved in Bets..."+bankScoreSavedInBets);
    }
}
