package PigGame;

import Libs.Form.Form;
import PigGame.Players.Computer;
import PigGame.Players.Player;
import PigGame.Players.User;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by becva on 14.09.2016.
 */
public class PigGame {
    private static Random rand = new Random();
    private static Form settingsForm;
    static {
        settingsForm = new Form();
        settingsForm.addNumber("speed","Insert speed of throwing in miliseconds")
            .setRange(100,500)
            .setDefaultValue("200");
        settingsForm.addNumber("computerStoreAt","Insert the amount of score when computer should save into the bank\n")
                .setRange(21,99)
                .setDefaultValue("21");
        settingsForm.askForAllFields();
        settingsForm.addNumber("winningScore","Insert winning score")
            .setRange(settingsForm.getNumber("computerStoreAt").getValue()+1,100)
            .setDefaultValue("100");
        settingsForm.askForField("winningScore");
    }
    public static int winningScore = (int) settingsForm.getNumber("winningScore").getValue();
    private static int round = 0;
    public static LinkedList<Player> players = new LinkedList<Player>();

    public static void main(String[] args) {
        initializeUsers();
        while (Player.winners.isEmpty()){
            round++;
            for (Player player : players) {
                if(player.initializePlayerRound(round)) {
                    for (int i = 1; i <= 2; i++) {
                        player.doTurn();
                        if (player.checkForInteruptionAndReport(i)) {
                            break;
                        }
                        i = i%2;
                    }
                }
            }
        }
        printStats();
        printWinners();
    }
    private static void initializeUsers() {
        Form playerForm = getPlayerForm();
        int computerStoreAt = (int) settingsForm.getNumber("computerStoreAt").getValue();
        Dice dice = new Dice((int) settingsForm.getNumber("speed").getValue());
        String continueAdding = "y";
        String playerType;
        System.out.println(".......................ADDING_PLAYERS...............................");
        do {
            playerType = playerForm.askForField("type");
            if(playerType.substring(0,1).equalsIgnoreCase("c")) {
                players.add(new Computer(dice,computerStoreAt));
            } else {
                String userName = playerForm.askForField("userName");
                players.add(new User(userName,dice));
            }
            System.out.println("Player named: "+players.getLast().name+" was added\n");
            if(players.size() >= 2) {
                continueAdding = playerForm.askForField("continueAdding");
            }
        } while (continueAdding.substring(0,1).equalsIgnoreCase("y"));
    }
    private static Form getPlayerForm() {
        Form playerForm = new Form();
        playerForm.addText("type","Select type of the player (computer/user)")
            .setAllowedValues(new String[]{"c","computer","user","u"})
            .setDefaultValue("c");
        playerForm.addText("userName","Insert user's name")
            .setDefaultValue("User");
        playerForm.addText("continueAdding","Do you want to continue adding users?")
                .setAllowedValues(new String[]{"y","n","Y","N","Yes","No","yes","no"})
                .setDefaultValue("n");
        return playerForm;
    }
    private static void printWinners() {
        System.out.println("\n****************************************************************\n");
        if(Player.winners.size() > 1) {
            System.out.println("It's a draw! Winners are:\n");
        } else {
            System.out.println("The ultimate winner is");
        }
        for (Player winner : Player.winners) {
            System.out.println(winner.name);
        }
        System.out.println("\n****************************************************************\n");
    }
    private static void printStats() {
        System.out.println("\n...............FINAL STATS for "+round+" rounds.................");
        for (Player player : players) {
            player.printStats();
        }
    }
}
