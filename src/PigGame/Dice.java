package PigGame;

import java.util.Random;

/**
 * Created by becva on 16.09.2016.
 */
public class Dice {
    private static Random rand = new Random();
    private int sleepRate;
    private int sides = 6;

    public Dice(int sleepRate) {
        this.sleepRate = sleepRate;
    }

    public int throwAndGetResult(int power) {
        int throwedNumber = 0;
        System.out.print("throwing....");
        for (int i = 0; i < power; i++) {
            throwedNumber = rand.nextInt(sides) + 1;
            sleep();
            if(i != power-1) {
                System.out.print(throwedNumber + "....");
            }
        }
        System.out.println("\nthrown number: "+throwedNumber);
        return throwedNumber;
    }
    private void sleep() {
        try {
            Thread.sleep(sleepRate);
        } catch(InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
